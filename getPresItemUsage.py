import logging
import json
import datetime
import traceback
import pymysql
import sys
import os


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error(
            "ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_CheckPrescriptionExists(lv_CutitronicsPresID):

    lv_list = ""
    lv_stmt = "SELECT * FROM BrandPrescriptions WHERE CutitronicsPresID = %(CutitronicsPresID)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_stmt, {'CutitronicsPresID': lv_CutitronicsPresID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))
            
            if len(lv_list) == 0:
                return 500
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def fn_getItemUsageDetails(lv_CutitronicsPresID, lv_CutitronicsPresDetID, lv_CutitronicsSKUCode):

    lv_list = ""
    lv_Stmt = "SELECT CutitronicsPresHDRID, CutitronicsPresDETID, CutitronicsSKUCode, PrescriptionType, Frequency, FrequencyType, UsageAM, UsagePM, RecommendedAmount FROM BrandPrescriptionItems WHERE CutitronicsPresHDRID = %(CutitronicsPresID)s AND CutitronicsPresDetID = %(CutitronicsPresDetID)s AND CutitronicsSKUCode = %(CutitronicsSKUCode)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_Stmt, {'CutitronicsPresID': lv_CutitronicsPresID, 'CutitronicsPresDetID': lv_CutitronicsPresDetID, 'CutitronicsSKUCode': lv_CutitronicsSKUCode})

            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def fn_CheckPrescriptionItemExists(lv_CutitronicsPresID, lv_CutitronicsPresDetID):

    lv_list = ""
    lv_stmt = "SELECT CutitronicsPresHDRID, CutitronicsPresDETID, CutitronicsSKUCode FROM BrandPrescriptionItems WHERE CutitronicsPresHDRID = %(CutitronicsPresID)s AND CutitronicsPresDETID = %(CutitronicsPresDetID)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_stmt, {'CutitronicsPresID': lv_CutitronicsPresID, 'CutitronicsPresDetID': lv_CutitronicsPresDetID})

            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))
            
            if len(lv_list) == 0:
                return 500
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500

def lambda_handler(event, context):
    """
    getSession

    """

    openConnection()
    logger.info(
        "Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_CutitronicsPresID = None

    lv_Body = event.get('multiValueQueryStringParameters')
    logger.info("Payload body - '{body}'".format(body=lv_Body))

    
    lv_CutitronicsPresID = lv_Body.get('CutitronicsPresID')
    lv_CutitronicsSKUCode = lv_Body.get('CutitronicsSKUCode')
    lv_CutitronicsPresDetID = lv_Body.get('CutitronicsPresDetID')

    # Return block

    getPresItem_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

    try:

        '''
        1. Does Prescriptions exist
        '''

        logger.info(" ")
        logger.info("Calling fn_CheckPrescriptionExists to check if the following Session exists - %s", lv_CutitronicsPresID)
        logger.info(" ")

        lv_PrescriptionItem = fn_CheckPrescriptionExists(lv_CutitronicsPresID)

        logger.info(" ")
        logger.info("fn_CheckPrescriptionExists return - %s", lv_PrescriptionItem)
        logger.info(" ")

        if lv_PrescriptionItem == 500:  # Failure

            logger.error('Prescription does not exist within database for prescription ''{lv_SessionID}'.format(lv_SessionID=lv_CutitronicsPresID))

            logger.info(" ")
            logger.info("Return to caller - %s", getPresItem_out)
            logger.info(" ")

            getPresItem_out['Status'] = "Error"
            getPresItem_out['ErrorCode'] = context.function_name + "_001"
            getPresItem_out['ErrorDesc'] = "Prescription does not exist within database"

            # Lambda response
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getPresItem_out)
            }

        '''
        2. Check PresDetID
        '''
        logger.info(" ")
        logger.info("Calling fn_CheckPrescriptionItemExists to check if the following Session exists - %s", lv_CutitronicsPresID)
        logger.info(" ")

        lv_PrescriptionItem = fn_CheckPrescriptionItemExists(lv_CutitronicsPresID, lv_CutitronicsPresDetID)

        logger.info(" ")
        logger.info("fn_CheckPrescriptionItemExists return - %s", lv_PrescriptionItem)
        logger.info(" ")

        if lv_PrescriptionItem == 500:  # Failure

            logger.error('Prescription item does not exist within database for prescription ''{lv_SessionID}'.format(lv_SessionID=lv_CutitronicsPresDetID))

            logger.info(" ")
            logger.info("Return to caller - %s", getPresItem_out)
            logger.info(" ")

            getPresItem_out['Status'] = "Error"
            getPresItem_out['ErrorCode'] = context.function_name + "_002"
            getPresItem_out['ErrorDesc'] = "Prescription item does not exist within database"

            # Lambda response
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getPresItem_out)
            }


        '''
        3. Get Item Usage
        '''

        logger.info(" ")
        logger.info("Calling fn_getItemUsageDetails to check if the following Item exists - %s", lv_CutitronicsPresID)
        logger.info(" ")

        lv_ItemUsageDet = fn_getItemUsageDetails(lv_CutitronicsPresID, lv_CutitronicsPresDetID, lv_CutitronicsSKUCode)

        logger.info(" ")
        logger.info("fn_getSessionsNotes return - %s", lv_ItemUsageDet)
        logger.info(" ")

        if lv_ItemUsageDet == 400:  # No session notes
            logger.warning("No prescription item '{lv_SKUCode}' found in the back office for prescription '{lv_CutitronicsPresID}'".format(lv_CutitronicsPresID=lv_CutitronicsPresID, lv_SKUCode=lv_CutitronicsSKUCode))

            logger.info(" ")
            logger.info("Return to caller - %s", getPresItem_out)
            logger.info(" ")

            getPresItem_out['Status'] = "Error"
            getPresItem_out['ErrorCode'] = context.function_name + "_003"
            getPresItem_out['ErrorDesc'] = "Prescription item does not exist within database"

            # Lambda response
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getPresItem_out)
            }
        else:
            logger.info("Found prescription item in back office systems")

        '''
        5. Build outPut
        '''
        try:
            logger.info("Building getSession_out reply block")

            getPresItem_out['ServiceOutput']['CutitronicsPresHDRID'] = lv_ItemUsageDet.get('CutitronicsPresHDRID')
            getPresItem_out['ServiceOutput']['CutitronicsPresDETID'] = lv_ItemUsageDet.get('CutitronicsPresDETID')
            getPresItem_out['ServiceOutput']['CutitronicsSKUCode'] = lv_ItemUsageDet.get('CutitronicsSKUCode')
            getPresItem_out['ServiceOutput']['PrescriptionType'] = lv_ItemUsageDet.get('PrescriptionType')
            getPresItem_out['ServiceOutput']['Frequency'] = lv_ItemUsageDet.get('Frequency')
            getPresItem_out['ServiceOutput']['FrequencyType'] = lv_ItemUsageDet.get('FrequencyType')
            getPresItem_out['ServiceOutput']['UsageAM'] = lv_ItemUsageDet.get('UsageAM')
            getPresItem_out['ServiceOutput']['UsagePM'] = lv_ItemUsageDet.get('UsagePM')
            getPresItem_out['ServiceOutput']['RecommendedAmount'] = lv_ItemUsageDet.get('RecommendedAmount')


        except Exception as e:
            logger.error(lv_Body)

        logger.info(" ")
        logger.info("Return to caller - %s", getPresItem_out)
        logger.info(" ")

        Connection.commit()
        Connection.close()

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getPresItem_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)

        # Populate Cutitronics reply block

        getPresItem_out['Status'] = "Error"
        getPresItem_out['ErrorCode'] = context.function_name + "_000"
        getPresItem_out['ErrorDesc'] = "CatchALL"

        # Lambda response
        Connection.rollback()
        Connection.close()

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getPresItem_out)
        }
